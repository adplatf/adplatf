package ru.serega6531.adplatf;

public enum ServiceStatus {

    DOWN, CORRUPT, MUMBLE, UP

}
