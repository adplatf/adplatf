package ru.serega6531.adplatf.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.serega6531.adplatf.entity.Flag;

import java.util.Optional;

public interface FlagRepository extends JpaRepository<Flag, String> {

    @Query("SELECT f FROM Flag f WHERE f.team = ?1 AND f.service = ?2 AND f.round = " +
            "(SELECT MAX(f.round) FROM Flag f WHERE f.team = ?1 AND f.service = ?2)")
    Optional<Flag> findLatestByTeamAndService(int team, int service);

}
