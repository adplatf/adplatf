package ru.serega6531.adplatf.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.serega6531.adplatf.entity.TeamServiceStatus;
import ru.serega6531.adplatf.entity.TeamServiceStatusId;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface TeamServiceStatusRepository extends JpaRepository<TeamServiceStatus, TeamServiceStatusId> {

    @Query("SELECT s FROM TeamServiceStatus s WHERE s.id.team = ?1 AND s.id.service = ?2 AND s.id.time = " +
            "(SELECT MAX(s.id.time) FROM TeamServiceStatus s WHERE s.id.team = ?1 AND s.id.service = ?2)")
    Optional<TeamServiceStatus> findLatestByTeamAndService(int team, int service);

    @Query("SELECT s FROM TeamServiceStatus s WHERE s.id.team = ?1 AND s.id.service = ?2 AND s.id.time < ?3")
    List<TeamServiceStatus> findStatuses(int team, int service, LocalDateTime time, Pageable pageable);

}
