package ru.serega6531.adplatf.configuration;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@SuppressWarnings({"unused", "RedundantSuppression"})
@Data
@NoArgsConstructor
public class PlatformConfiguration {

    private String ctfTitle;

    private int checkInterval;   // in seconds
    private int checksInRound;   // so checkInterval * checksInRound = round length
    private int totalRounds;
    private int flagLifeLength;  // rounds before the flag expire
    private int queueTtl;        // for how long can a check message wait for processing
    private LocalDateTime gameStartTime;
    private LocalDateTime freezeStartTime;  // could be null for no freeze time

    private boolean serviceMustBeUpToSubmit;
    private int scoreHardness;
    private boolean scoreInflation;

    private List<Team> teams;
    private List<Service> services;

    @Data
    @NoArgsConstructor
    public static class Team {
        private String name;
        private String ip;
        private String token;
        private String icon;
    }

    @Data
    @NoArgsConstructor
    public static class Service {
        private String name;
        private double initialScore;
        private double firstBloodBonus;  // could be zero

        // Checkers execution scripts, i.e. "/checker/check.sh" or "python checker.py --custom-param"
        private String checkFunctionalityScript;  // redis host, password and ip are appended to arguments
        private String pushFlagScript;            // redis host, password, ip and old flag are appended to arguments
        private String pullFlagScript;            // redis host, password, ip and new flag are appended to arguments
    }

}
