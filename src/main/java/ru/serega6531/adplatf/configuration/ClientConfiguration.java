package ru.serega6531.adplatf.configuration;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.OffsetDateTime;
import java.util.List;

@Data
@AllArgsConstructor
public class ClientConfiguration {

    private String ctfTitle;
    private OffsetDateTime gameStartTime;
    private int totalRounds;
    private int roundLength;
    private List<String> services;
    private List<ClientTeam> teams;

    @Data
    @AllArgsConstructor
    static class ClientTeam {
        private String name;
        private String icon;
        private String ip;
    }

}
