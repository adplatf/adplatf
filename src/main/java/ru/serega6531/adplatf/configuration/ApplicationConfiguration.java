package ru.serega6531.adplatf.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.Declarables;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.AsyncRabbitTemplate;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.Protocol;
import ru.serega6531.adplatf.controller.SubscribingWebSocketHandler;

import java.io.File;
import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Configuration
@EnableWebSocket
@EnableScheduling
@EnableCaching
public class ApplicationConfiguration implements WebSocketConfigurer {

    private final SubscribingWebSocketHandler webSocketHandler;

    @Autowired
    public ApplicationConfiguration(SubscribingWebSocketHandler webSocketHandler) {
        this.webSocketHandler = webSocketHandler;
    }

    @Bean
    public PlatformConfiguration platformConfiguration(ObjectMapper objectMapper) throws IOException {
        File configFile = new File("config.json");

        if (!configFile.exists()) {
            throw new IllegalStateException("config.json does not exist in " + new File(".").getAbsolutePath());
        }

        return objectMapper.readValue(configFile, PlatformConfiguration.class);
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public ClientConfiguration clientConfiguration(PlatformConfiguration configuration) {
        int totalRounds = configuration.getTotalRounds();
        int roundLength = configuration.getChecksInRound() * configuration.getCheckInterval();

        List<String> services = configuration.getServices().stream()
                .map(PlatformConfiguration.Service::getName)
                .collect(Collectors.toList());

        List<ClientConfiguration.ClientTeam> teams = configuration.getTeams().stream()
                .map(t -> new ClientConfiguration.ClientTeam(t.getName(), t.getIcon(), t.getIp()))
                .collect(Collectors.toList());

        ZoneOffset offset = OffsetDateTime.now().getOffset();
        return new ClientConfiguration(configuration.getCtfTitle(),
                configuration.getGameStartTime().atOffset(offset),
                totalRounds, roundLength, services, teams);
    }

    @Bean
    public JedisPool jedisPool(@Value("${REDIS_PASSWORD}") String password) {
        return new JedisPool(new JedisPoolConfig(), "redis", Protocol.DEFAULT_PORT, Protocol.DEFAULT_TIMEOUT, password);
    }

    @Bean
    AsyncRabbitTemplate asyncRabbitTemplate(RabbitTemplate rabbitTemplate, PlatformConfiguration configuration) {
        AsyncRabbitTemplate template = new AsyncRabbitTemplate(rabbitTemplate);
        template.setReceiveTimeout(TimeUnit.SECONDS.toMillis(configuration.getCheckInterval()));
        return template;
    }

    @Bean
    public Declarables qs(PlatformConfiguration configuration) {
        return new Declarables(new Queue("checker_input",
                true, false, false,
                Map.of("x-message-ttl", TimeUnit.SECONDS.toMillis(configuration.getQueueTtl()))));
    }

    @Bean
    public TaskScheduler sockJsTaskScheduler() {
        ThreadPoolTaskScheduler threadPoolScheduler = new ThreadPoolTaskScheduler();

        threadPoolScheduler.setThreadNamePrefix("SockJS-");
        threadPoolScheduler.setPoolSize(Runtime.getRuntime().availableProcessors());
        threadPoolScheduler.setRemoveOnCancelPolicy(true);

        return threadPoolScheduler;
    }

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(webSocketHandler, "/ws").setAllowedOrigins("*");
    }
}
