package ru.serega6531.adplatf;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FlagSubmissionResult {

    private String flag;
    private String msg;

}
