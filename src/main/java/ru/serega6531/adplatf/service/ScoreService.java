package ru.serega6531.adplatf.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import ru.serega6531.adplatf.configuration.PlatformConfiguration;
import ru.serega6531.adplatf.controller.pojo.ServiceInfo;
import ru.serega6531.adplatf.ws.*;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
public class ScoreService {

    private final SubscriptionService subscriptionService;
    private final JedisPool jedisPool;
    private final CachingRedisRepository redisRepository;
    private final PlatformConfiguration configuration;

    @Autowired
    public ScoreService(SubscriptionService subscriptionService,
                        JedisPool jedisPool, CachingRedisRepository redisRepository, PlatformConfiguration configuration) {
        this.subscriptionService = subscriptionService;
        this.jedisPool = jedisPool;
        this.redisRepository = redisRepository;
        this.configuration = configuration;
    }

    @PostConstruct
    public void init() {
        List<PlatformConfiguration.Team> teams = configuration.getTeams();
        List<PlatformConfiguration.Service> services = configuration.getServices();

        try (Jedis jedis = jedisPool.getResource()) {
            for (int team = 0; team < teams.size(); team++) {
                for (int service = 0; service < services.size(); service++) {
                    PlatformConfiguration.Service serviceConfig = services.get(service);
                    redisRepository.initScoreDefaults(team, service, serviceConfig.getInitialScore(), jedis);
                }
            }
        }
    }

    public Map<Integer, Integer> getFirstBloods() {
        List<PlatformConfiguration.Service> services = configuration.getServices();
        Map<Integer, Integer> result = new HashMap<>();

        for (int service = 0; service < services.size(); service++) {
            Optional<Integer> opt = redisRepository.getFirstBloodTeam(service);
            if (opt.isPresent()) {
                result.put(service, opt.get());
            }
        }

        return result;
    }

    /**
     * @return Map&lt;Team, Map&lt;Service, ServiceInfo&gt;&gt;
     */
    public Map<Integer, Map<Integer, ServiceInfo>> getAllScores(boolean bypassFreeze) {
        List<PlatformConfiguration.Team> teams = configuration.getTeams();
        List<PlatformConfiguration.Service> services = configuration.getServices();
        Map<Integer, Map<Integer, ServiceInfo>> result = new HashMap<>();

        boolean freeze = redisRepository.isFreeze();
        boolean frozen = freeze && !bypassFreeze;

        for (int team = 0; team < teams.size(); team++) {
            Map<Integer, ServiceInfo> teamResult = new HashMap<>();
            result.put(team, teamResult);

            for (int service = 0; service < services.size(); service++) {
                if (frozen) {
                    teamResult.put(service, getFrozenServiceInfo(team, service));
                } else {
                    teamResult.put(service, getServiceInfo(team, service));
                }
            }
        }

        return result;
    }

    private ServiceInfo getFrozenServiceInfo(int team, int service) {
        double score = redisRepository.getFrozenScore(team, service);
        int stolenFlags = redisRepository.getFrozenStolenFlags(team, service);
        int lostFlags = redisRepository.getFrozenLostFlags(team, service);

        return new ServiceInfo(score, stolenFlags, lostFlags);
    }

    private ServiceInfo getServiceInfo(int team, int service) {
        double score = redisRepository.getScore(team, service);
        int stolenFlags = redisRepository.getStolenFlags(team, service);
        int lostFlags = redisRepository.getLostFlags(team, service);

        return new ServiceInfo(score, stolenFlags, lostFlags);
    }

    /**
     * @return Pair of attacker delta and victim delta
     */
    public Pair<Double, Double> stealFlag(int attacker, int victim, int service) {
        String attackerName = configuration.getTeams().get(attacker).getName();
        String victimName = configuration.getTeams().get(victim).getName();
        String serviceName = configuration.getServices().get(service).getName();

        try (Jedis jedis = jedisPool.getResource()) {
            double attackerScore = redisRepository.getScore(attacker, service);
            double victimScore = redisRepository.getScore(victim, service);

            Pair<Double, Double> newScores = calculateNewScores(attackerScore, victimScore);
            Double newAttackerScore = newScores.getLeft();
            Double newVictimScore = newScores.getRight();

            if (redisRepository.getFirstBloodTeam(service).isEmpty()) {
                newAttackerScore += configuration.getServices().get(service).getFirstBloodBonus();
                log.info("Team {} got bonus points for first blood on service {}", attackerName, serviceName);

                // Even when freeze is on, we don't care
                subscriptionService.broadcast(new SubscriptionMessage(SubscriptionMessageType.FIRST_BLOOD,
                        new FirstBloodMessageContent(attacker, service)));

                redisRepository.setFirstBloodTeam(service, attacker, jedis);
            }

            redisRepository.setScore(attacker, service, newAttackerScore, jedis);
            redisRepository.setScore(victim, service, newVictimScore, jedis);

            redisRepository.incrementStolenFlags(attacker, service, jedis);
            redisRepository.incrementLostFlags(victim, service, jedis);

            double attackerDelta = newAttackerScore - attackerScore;
            double victimDelta = newVictimScore - victimScore;

            if (!redisRepository.isFreeze()) {
                subscriptionService.broadcast(new SubscriptionMessage(SubscriptionMessageType.UPDATE_SCORE,
                        new UpdateScoreMessageContent(attacker, service, newAttackerScore)));
                subscriptionService.broadcast(new SubscriptionMessage(SubscriptionMessageType.UPDATE_SCORE,
                        new UpdateScoreMessageContent(victim, service, newVictimScore)));
                subscriptionService.broadcast(new SubscriptionMessage(SubscriptionMessageType.STEAL_FLAG,
                        new StealFlagMessageContent(attacker, victim, service, attackerDelta, victimDelta)));
            }

            log.info("Team {} stole the flag from team {}, service {}. Deltas are {}, {}, new scores are {}, {}",
                    attackerName, victimName, serviceName, attackerDelta, victimDelta, newAttackerScore, newVictimScore);
            return Pair.of(attackerDelta, victimDelta);
        }
    }

    /**
     * Original algorithm: https://github.com/pomo-mondreganto/ForcAD/blob/master/backend/scripts/create_functions.sql
     */
    private Pair<Double, Double> calculateNewScores(double attackerScore, double victimScore) {
        int hardness = configuration.getScoreHardness();
        boolean inflation = configuration.isScoreInflation();

        double scale = 50 * Math.sqrt(hardness);
        double norm = Math.log(Math.log(hardness)) / 12;

        double attackerDelta = scale / (1 + Math.exp((Math.sqrt(attackerScore) - Math.sqrt(victimScore)) * norm));

        double victimDelta = -Math.min(victimScore, attackerDelta);
        if (Math.min(victimScore, attackerDelta) == 0) {
            victimDelta = 0;
        }

        if (!inflation) {
            attackerDelta = Math.min(attackerDelta, -victimDelta);
        }

        double newAttackerScore = attackerScore + attackerDelta;
        double newVictimScore = Math.max(0, victimScore + victimDelta);  // in case of rounding errors

        return Pair.of(newAttackerScore, newVictimScore);
    }

}
