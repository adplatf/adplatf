package ru.serega6531.adplatf.service;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpReplyTimeoutException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.AsyncRabbitTemplate;
import org.springframework.stereotype.Service;
import ru.serega6531.adplatf.amqp.CheckAction;
import ru.serega6531.adplatf.amqp.CheckCallback;
import ru.serega6531.adplatf.amqp.CheckerRequest;
import ru.serega6531.adplatf.amqp.CheckerResult;
import ru.serega6531.adplatf.configuration.PlatformConfiguration;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.concurrent.CancellationException;

@Service
@Slf4j
public class RunnerService {

    private final PlatformConfiguration configuration;
    private final AsyncRabbitTemplate rabbitTemplate;
    private final MetricsService metricsService;
    private final StatusService statusService;

    public RunnerService(PlatformConfiguration configuration, AsyncRabbitTemplate rabbitTemplate,
                         MetricsService metricsService, StatusService statusService) {
        this.configuration = configuration;
        this.rabbitTemplate = rabbitTemplate;
        this.metricsService = metricsService;
        this.statusService = statusService;
    }

    public void execute(int team, int service,
                        CheckAction action, boolean expectedPush, CheckCallback callback, String flag) {
        AsyncRabbitTemplate.RabbitConverterFuture<CheckerResult> future = rabbitTemplate.convertSendAndReceive(
                "checker_input",
                new CheckerRequest(team, service, action, expectedPush, LocalDateTime.now(), flag));
        future.addCallback(c -> processResponse(Objects.requireNonNull(c), callback),
                this::processFailure);
    }

    private void processResponse(CheckerResult checkerResult, CheckCallback callback) {
        String resultOut = checkerResult.getExecutionOut();
        String resultErr = checkerResult.getExecutionErr();
        boolean isError = checkerResult.isError();
        boolean expectedPush = checkerResult.isExpectedPush();

        resultOut = statusService.trimOutput(resultOut);

        int team = checkerResult.getTeam();
        int service = checkerResult.getService();
        CheckAction action = checkerResult.getAction();
        int executionTime = checkerResult.getExecutionTime();

        LocalDateTime requestTime = checkerResult.getRequestTime();
        LocalDateTime currentTime = LocalDateTime.now();
        int responseTime = (int) Duration.between(requestTime, currentTime).toMillis();

        metricsService.recordExecutionTime(team, service, action, executionTime);
        metricsService.recordResponseTime(responseTime);

        String teamName = configuration.getTeams().get(team).getName();
        String serviceName = configuration.getServices().get(service).getName();

        if (isError) {
            log.error("Runner for team {}, service {}, action {} returned error: \"{}\", \"{}\"",
                    teamName, serviceName, action.getName(),
                    statusService.cleanOutput(resultOut),
                    statusService.trimOutput(statusService.cleanOutput(resultErr)));
        }

        callback.call(team, service, resultOut, resultErr, expectedPush, requestTime);
    }

    private void processFailure(Throwable e) {
        if (e instanceof CancellationException) {
            return;
        }

        if (e instanceof AmqpReplyTimeoutException) {
            Message message = ((AmqpReplyTimeoutException) e).getRequestMessage();
            byte[] body = message.getBody();
            CheckerRequest req = deserializeRequest(body);

            int team = req.getTeam();
            int service = req.getService();

            String teamName = configuration.getTeams().get(team).getName();
            String serviceName = configuration.getServices().get(service).getName();

            log.error("Timeout while waiting for reply: {} to team {}, service {}. Request time was {}",
                    req.getAction(), teamName, serviceName, req.getRequestTime());
            metricsService.incrementCheckerTimeouts();
            return;
        }

        log.error("Error in future", e);
    }

    @SneakyThrows
    private CheckerRequest deserializeRequest(byte[] body) {
        ObjectInputStream is = new ObjectInputStream(new ByteArrayInputStream(body));
        return (CheckerRequest) is.readObject();
    }

}
