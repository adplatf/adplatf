package ru.serega6531.adplatf.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.serega6531.adplatf.configuration.PlatformConfiguration;
import ru.serega6531.adplatf.entity.Flag;
import ru.serega6531.adplatf.repository.FlagRepository;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

@Service
@Slf4j
public class FlagService {

    public static final int FLAG_LENGTH = 32;
    public static final char[] ALPHABET = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();  // must be sorted

    static {
        if (!ArrayUtils.isSorted(ALPHABET)) {
            throw new AssertionError("ALPHABET is not sorted");
        }
    }

    private final FlagRepository repository;
    private final CachingRedisRepository redisRepository;
    private final PlatformConfiguration platformConfiguration;

    // Map<Pair<Team, Service>, Flag>
    private final Map<Pair<Integer, Integer>, Flag> currentFlags = new ConcurrentHashMap<>();

    @Autowired
    public FlagService(FlagRepository repository, CachingRedisRepository redisRepository, PlatformConfiguration platformConfiguration) {
        this.repository = repository;
        this.redisRepository = redisRepository;
        this.platformConfiguration = platformConfiguration;
    }

    @PostConstruct
    public void init() {
        List<PlatformConfiguration.Team> teams = platformConfiguration.getTeams();
        List<PlatformConfiguration.Service> services = platformConfiguration.getServices();

        for (int team = 0; team < teams.size(); team++) {
            for (int service = 0; service < services.size(); service++) {
                Optional<Flag> flag = loadCurrentFlag(team, service);
                if (flag.isPresent()) {
                    currentFlags.put(ImmutablePair.of(team, service), flag.get());
                }
            }
        }
    }

    private Flag generateFlag(int team, int service) {
        String text = generateFlagText();
        int round = redisRepository.getRoundNum();

        Flag flag = new Flag();
        flag.setText(text);
        flag.setTeam(team);
        flag.setService(service);
        flag.setRound(round);
        flag.setPushed(false);

        flag = repository.save(flag);
        currentFlags.put(ImmutablePair.of(team, service), flag);

        String teamName = platformConfiguration.getTeams().get(team).getName();
        String serviceName = platformConfiguration.getServices().get(service).getName();

        log.info("[ROUND {}] Generated flag {} for team {}, service {}", round, text, teamName, serviceName);

        return flag;
    }

    public Optional<Flag> getFlag(String text) {
        return repository.findById(text);
    }

    public void redeemFlag(Flag flag, int team) {
        log.debug("Marking flag {} as redeemed", flag.getText());
        flag.getRedeems().add(team);
        repository.save(flag);
    }

    public void markFlagPushed(Flag flag) {
        log.debug("Marking flag {} as pushed", flag.getText());
        flag.setPushed(true);
        repository.save(flag);
    }

    public Optional<Flag> getCurrentFlag(int team, int service) {
        return Optional.ofNullable(currentFlags.get(ImmutablePair.of(team, service)));
    }

    public Flag getCurrentFlagOrGenerate(int team, int service, int round) {
        Optional<Flag> flag = getCurrentFlag(team, service);
        return flag.filter(f -> f.getRound() == round).orElseGet(() -> generateFlag(team, service));
    }

    private Optional<Flag> loadCurrentFlag(int team, int service) {
        return repository.findLatestByTeamAndService(team, service);
    }

    private String generateFlagText() {
        while (true) {
            StringBuilder sb = new StringBuilder(FLAG_LENGTH);

            ThreadLocalRandom.current().ints(FLAG_LENGTH - 1, 0, ALPHABET.length)
                    .forEach(i -> sb.append(ALPHABET[i]));

            sb.append('=');
            String text = sb.toString();

            if (!repository.existsById(text)) {
                return text;
            }
        }
    }

    public boolean isValidFlagFormat(String flag) {
        if (flag.length() != FLAG_LENGTH) {
            return false;
        }

        for (int i = 0; i < FLAG_LENGTH - 1; i++) {
            if (!isInAlphabet(flag.charAt(i))) {
                return false;
            }
        }

        return flag.charAt(FLAG_LENGTH - 1) == '=';
    }

    private boolean isInAlphabet(char c) {
        return Arrays.binarySearch(ALPHABET, c) >= 0;
    }

}
