package ru.serega6531.adplatf.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import ru.serega6531.adplatf.ws.SubscriptionMessage;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Slf4j
@Service
public class SubscriptionService {

    private final List<WebSocketSession> subscribers = new CopyOnWriteArrayList<>();

    private final ObjectMapper mapper;

    @Autowired
    public SubscriptionService(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    public void addSubscriber(WebSocketSession session) {
        subscribers.add(session);
        log.info("[WS] User subscribed: {}", session.getId());
    }

    public void removeSubscriber(WebSocketSession session) {
        subscribers.remove(session);
        log.info("[WS] User unsubscribed: {}", session.getId());
    }

    public void broadcast(SubscriptionMessage message) {
        try {
            final TextMessage messageJson = objectToTextMessage(message);
            broadcast(messageJson);
        } catch (JsonProcessingException e) {
            log.warn("JSON", e);   // should never happen, but what if...
        }
    }

    @Synchronized
    private void broadcast(TextMessage messageJson) {
        subscribers.forEach(s -> {
            try {
                s.sendMessage(messageJson);
            } catch (Exception e) {
                log.warn("WS", e);
            }
        });
    }

    private TextMessage objectToTextMessage(SubscriptionMessage object) throws JsonProcessingException {
        return new TextMessage(mapper.writeValueAsString(object));
    }


}
