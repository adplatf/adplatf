package ru.serega6531.adplatf.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Optional;

@Service
@Slf4j
public class CachingRedisRepository {

    private final JedisPool jedisPool;

    @Autowired
    public CachingRedisRepository(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

    public void initStateDefaults() {
        try (Jedis jedis = jedisPool.getResource()) {
            // Setting defaults
            jedis.setnx("state/gameStarted", "false");
            jedis.setnx("state/gameEnded", "false");
            jedis.setnx("state/roundNum", "1");
            jedis.setnx("state/currentCheckInRound", "0");
            jedis.setnx("state/freeze", "false");
        }
    }

    @Cacheable(value = "state", key = "'gameStarted'")
    public boolean isGameStarted() {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.get("state/gameStarted").equals("true");
        }
    }

    @CacheEvict(value = "state", key = "'gameStarted'")
    public void setGameStarted(boolean value) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set("state/gameStarted", Boolean.toString(value));
        }
    }

    @Cacheable(value = "state", key = "'gameEnded'")
    public boolean isGameEnded() {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.get("state/gameEnded").equals("true");
        }
    }

    @CacheEvict(value = "state", key = "'gameEnded'")
    public void setGameEnded(boolean value) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set("state/gameEnded", Boolean.toString(value));
        }
    }

    @Cacheable(value = "state", key = "'roundNum'")
    public int getRoundNum() {
        try (Jedis jedis = jedisPool.getResource()) {
            return Integer.parseInt(jedis.get("state/roundNum"));
        }
    }

    @CacheEvict(value = "state", key = "'roundNum'")
    public int incrementRoundNum() {
        try (Jedis jedis = jedisPool.getResource()) {
            return Math.toIntExact(jedis.incr("state/roundNum"));
        }
    }

    @CacheEvict(value = "state", key = "'roundNum'")
    public void setRoundNum(int value) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set("state/roundNum", Integer.toString(value));
        }
    }

    @Cacheable(value = "state", key = "'currentCheckInRound'")
    public int getCurrentCheckInRound() {
        try (Jedis jedis = jedisPool.getResource()) {
            return Integer.parseInt(jedis.get("state/currentCheckInRound"));
        }
    }

    @CacheEvict(value = "state", key = "'currentCheckInRound'")
    public void setCurrentCheckInRound(int value) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set("state/currentCheckInRound", Integer.toString(value));
        }
    }

    @Cacheable(value = "state", key = "'freeze'")
    public boolean isFreeze() {
        try (Jedis jedis = jedisPool.getResource()) {
            return jedis.get("state/freeze").equals("true");
        }
    }

    @CacheEvict(value = "state", key = "'freeze'")
    public void setFreeze(boolean value) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set("state/freeze", Boolean.toString(value));
        }
    }

    public void initScoreDefaults(int team, int service, double initialScore, Jedis jedis) {
        jedis.setnx(String.format("score/%d/%d", team, service), String.valueOf(initialScore));
        jedis.setnx(String.format("flags/stolen/%d/%d", team, service), "0");
        jedis.setnx(String.format("flags/lost/%d/%d", team, service), "0");
    }

    @CacheEvict(value = {"frozenScore", "frozenStolenFlags", "frozenLostFlags"}, allEntries = true)
    public void copyScoreDataToFrozen(int team, int service, Jedis jedis) {
        String score = jedis.get(String.format("score/%s/%s", team, service));
        jedis.set(String.format("frozenScore/%s/%s", team, service), score);

        String stolenFlags = jedis.get(String.format("flags/stolen/%s/%s", team, service));
        jedis.set(String.format("frozenFlags/stolen/%s/%s", team, service), stolenFlags);

        String lostFlags = jedis.get(String.format("flags/lost/%s/%s", team, service));
        jedis.set(String.format("frozenFlags/lost/%s/%s", team, service), lostFlags);
    }

    @CacheEvict(value = "score", key = "'team:' + #team + ';service:' + #service")
    public void setScore(int team, int service, double amount, Jedis jedis) {
        jedis.set(String.format("score/%d/%d", team, service), String.valueOf(amount));
    }

    @CacheEvict(value = "score", key = "'team:' + #team + ';service:' + #service")
    public void setScore(int team, int service, double amount) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set(String.format("score/%d/%d", team, service), String.valueOf(amount));
        }
    }

    @Cacheable(value = "frozenStolenFlags", key = "'team:' + #team + ';service:' + #service")
    public int getFrozenStolenFlags(int team, int service) {
        try (Jedis jedis = jedisPool.getResource()) {
            return Integer.parseInt(jedis.get(String.format("frozenFlags/stolen/%d/%d", team, service)));
        }
    }

    @Cacheable(value = "stolenFlags", key = "'team:' + #team + ';service:' + #service")
    public int getStolenFlags(int team, int service) {
        try (Jedis jedis = jedisPool.getResource()) {
            return Integer.parseInt(jedis.get(String.format("flags/stolen/%d/%d", team, service)));
        }
    }

    @CacheEvict(value = "stolenFlags", key = "'team:' + #team + ';service:' + #service")
    public void incrementStolenFlags(int team, int service, Jedis jedis) {
        jedis.incr(String.format("flags/stolen/%d/%d", team, service));
    }

    @CacheEvict(value = "stolenFlags", key = "'team:' + #team + ';service:' + #service")
    public void setStolenFlags(int team, int service, int amount) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set(String.format("flags/stolen/%d/%d", team, service), String.valueOf(amount));
        }
    }

    @Cacheable(value = "frozenLostFlags", key = "'team:' + #team + ';service:' + #service")
    public int getFrozenLostFlags(int team, int service) {
        try (Jedis jedis = jedisPool.getResource()) {
            return Integer.parseInt(jedis.get(String.format("frozenFlags/lost/%d/%d", team, service)));
        }
    }

    @Cacheable(value = "lostFlags", key = "'team:' + #team + ';service:' + #service")
    public int getLostFlags(int team, int service) {
        try (Jedis jedis = jedisPool.getResource()) {
            return Integer.parseInt(jedis.get(String.format("flags/lost/%d/%d", team, service)));
        }
    }

    @CacheEvict(value = "lostFlags", key = "'team:' + #team + ';service:' + #service")
    public void incrementLostFlags(int team, int service, Jedis jedis) {
        jedis.incr(String.format("flags/lost/%d/%d", team, service));
    }

    @CacheEvict(value = "lostFlags", key = "'team:' + #team + ';service:' + #service")
    public void setLostFlags(int team, int service, int amount) {
        try (Jedis jedis = jedisPool.getResource()) {
            jedis.set(String.format("flags/lost/%d/%d", team, service), String.valueOf(amount));
        }
    }

    @CacheEvict(value = "firstBloods", key = "#service")
    public void setFirstBloodTeam(int service, int team, Jedis jedis) {
        jedis.set(String.format("firstBloods/%d", service), String.valueOf(team));
    }

    @Cacheable(value = "firstBloods", key = "#service")
    public Optional<Integer> getFirstBloodTeam(int service) {
        try (Jedis jedis = jedisPool.getResource()) {
            String firstBloodTeam = jedis.get(String.format("firstBloods/%d", service));
            return Optional.ofNullable(firstBloodTeam).map(Integer::parseInt);
        }
    }

    @Cacheable(value = "score", key = "'team:' + #team + ';service:' + #service")
    public double getScore(int team, int service) {
        try (Jedis jedis = jedisPool.getResource()) {
            return Double.parseDouble(jedis.get(String.format("score/%d/%d", team, service)));
        }
    }

    @Cacheable(value = "frozenScore", key = "'team:' + #team + ';service:' + #service")
    public double getFrozenScore(int team, int service) {
        try (Jedis jedis = jedisPool.getResource()) {
            return Double.parseDouble(jedis.get(String.format("frozenScore/%d/%d", team, service)));
        }
    }

    public void initStatusDefaults(int team, int service, Jedis jedis) {
        jedis.setnx(String.format("sla/%d/%d/successfulChecks", team, service), "0");
        jedis.setnx(String.format("sla/%d/%d/totalChecks", team, service), "0");
    }

    @CachePut(value = "successfulChecks", key = "'team:' + #team + ';service:' + #service")
    public Long incrementSuccessfulChecks(int team, int service, Jedis jedis) {
        return jedis.incr(String.format("sla/%d/%d/successfulChecks", team, service));
    }

    @Cacheable(value = "successfulChecks", key = "'team:' + #team + ';service:' + #service")
    public Long getSuccessfulChecks(int team, int service) {
        try (Jedis jedis = jedisPool.getResource()) {
            return Long.parseLong(jedis.get(String.format("sla/%d/%d/successfulChecks", team, service)));
        }
    }

    @Cacheable(value = "frozenSuccessfulChecks", key = "'team:' + #team + ';service:' + #service")
    public Long getFrozenSuccessfulChecks(int team, int service) {
        try (Jedis jedis = jedisPool.getResource()) {
            return Long.parseLong(jedis.get(String.format("frozenSla/%d/%d/successfulChecks", team, service)));
        }
    }

    @CachePut(value = "totalChecks", key = "'team:' + #team + ';service:' + #service")
    public Long incrementTotalChecks(int team, int service, Jedis jedis) {
        return jedis.incr(String.format("sla/%d/%d/totalChecks", team, service));
    }

    @Cacheable(value = "totalChecks", key = "'team:' + #team + ';service:' + #service")
    public Long getTotalChecks(int team, int service) {
        try (Jedis jedis = jedisPool.getResource()) {
            return Long.parseLong(jedis.get(String.format("sla/%d/%d/totalChecks", team, service)));
        }
    }

    @Cacheable(value = "frozenTotalChecks", key = "'team:' + #team + ';service:' + #service")
    public Long getFrozenTotalChecks(int team, int service) {
        try (Jedis jedis = jedisPool.getResource()) {
            return Long.parseLong(jedis.get(String.format("frozenSla/%d/%d/totalChecks", team, service)));
        }
    }

    @CacheEvict(value = {"frozenSuccessfulChecks", "frozenTotalChecks"}, allEntries = true)
    public void copyChecksToFrozen(int team, int service, Jedis jedis) {
        String successfulChecks = jedis.get(String.format("sla/%d/%d/successfulChecks", team, service));
        jedis.set(String.format("frozenSla/%d/%d/successfulChecks", team, service), successfulChecks);

        String totalChecks = jedis.get(String.format("sla/%d/%d/totalChecks", team, service));
        jedis.set(String.format("frozenSla/%d/%d/totalChecks", team, service), totalChecks);
    }

}
