package ru.serega6531.adplatf.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import ru.serega6531.adplatf.configuration.PlatformConfiguration;
import ru.serega6531.adplatf.controller.pojo.GameState;
import ru.serega6531.adplatf.ws.NewRoundMessageContent;
import ru.serega6531.adplatf.ws.SubscriptionMessage;
import ru.serega6531.adplatf.ws.SubscriptionMessageType;
import ru.serega6531.adplatf.ws.UpdateScoreMessageContent;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;

@Service
@Slf4j
public class GameStateService {

    private final JedisPool jedisPool;
    private final CachingRedisRepository redisRepository;
    private final SubscriptionService subscriptionService;
    private final CheckerService checkerService;
    private final PlatformConfiguration configuration;

    private LocalDateTime currentRoundStart = null;
    private int forcedNextRound = -1;

    @Autowired
    public GameStateService(JedisPool jedisPool, CachingRedisRepository redisRepository,
                            SubscriptionService subscriptionService, CheckerService checkerService,
                            PlatformConfiguration configuration) {
        this.jedisPool = jedisPool;
        this.redisRepository = redisRepository;
        this.subscriptionService = subscriptionService;
        this.checkerService = checkerService;
        this.configuration = configuration;
    }

    @PostConstruct
    public void init() {
        redisRepository.initStateDefaults();
    }

    public void startGame() {
        if (!redisRepository.isGameStarted()) {
            log.info("GAME STARTED!");

            redisRepository.setGameStarted(true);
            currentRoundStart = LocalDateTime.now();

            redisRepository.setCurrentCheckInRound(1);

            subscriptionService.broadcast(new SubscriptionMessage(SubscriptionMessageType.GAME_START, null));
            subscriptionService.broadcast(new SubscriptionMessage(SubscriptionMessageType.NEW_ROUND,
                    new NewRoundMessageContent(
                            redisRepository.getRoundNum(),
                            getOffsetCurrentRoundStart())));

            checkerService.runChecks(true);
        }
    }

    public void endGame() {
        if (!redisRepository.isGameEnded()) {
            log.info("GAME ENDED!");

            redisRepository.setGameEnded(true);
            subscriptionService.broadcast(new SubscriptionMessage(SubscriptionMessageType.GAME_END, null));
        }
    }

    public void startFreeze() {
        if (!redisRepository.isFreeze()) {
            log.info("FREEZE STARTED");

            redisRepository.setFreeze(true);
            saveFrozenData();
            subscriptionService.broadcast(new SubscriptionMessage(SubscriptionMessageType.FREEZE_START, null));
        }
    }

    public void stopFreeze() {
        if (redisRepository.isFreeze()) {
            log.info("FREEZE STOPPED");

            redisRepository.setFreeze(false);
            sendUpdateState();
        }
    }

    public void forceSetScore(int team, int service, double newScore) {
        redisRepository.setScore(team, service, newScore);
        if (!redisRepository.isFreeze()) {
            subscriptionService.broadcast(new SubscriptionMessage(SubscriptionMessageType.UPDATE_SCORE,
                    new UpdateScoreMessageContent(team, service, newScore)));
        }
    }

    public void forceSetStolenFlags(int team, int service, int newAmount) {
        redisRepository.setStolenFlags(team, service, newAmount);
        if (!redisRepository.isFreeze()) {
            sendUpdateState();
        }
    }

    public void forceSetLostFlags(int team, int service, int newAmount) {
        redisRepository.setLostFlags(team, service, newAmount);
        if (!redisRepository.isFreeze()) {
            sendUpdateState();
        }
    }

    public void forceSetNextRound(int nextRound) {
        this.forcedNextRound = nextRound;
        log.info("Forcing next round to be {}", nextRound);
    }

    public void beginNewRound() {
        redisRepository.setCurrentCheckInRound(1);
        int newRound;

        if (forcedNextRound != -1) {
            newRound = forcedNextRound;
            forcedNextRound = -1;
            redisRepository.setRoundNum(newRound);
        } else {
            newRound = redisRepository.incrementRoundNum();
        }

        log.info("Starting round {}", newRound);
        currentRoundStart = LocalDateTime.now();

        subscriptionService.broadcast(new SubscriptionMessage(SubscriptionMessageType.NEW_ROUND,
                new NewRoundMessageContent(
                        redisRepository.getRoundNum(),
                        getOffsetCurrentRoundStart())));
        checkerService.runChecks(true);
    }

    private void saveFrozenData() {
        List<PlatformConfiguration.Team> teams = configuration.getTeams();
        List<PlatformConfiguration.Service> services = configuration.getServices();

        try (Jedis jedis = jedisPool.getResource()) {
            for (int team = 0; team < teams.size(); team++) {
                for (int service = 0; service < services.size(); service++) {
                    redisRepository.copyScoreDataToFrozen(team, service, jedis);
                    redisRepository.copyChecksToFrozen(team, service, jedis);
                }
            }
        }
    }

    public void sendUpdateState() {
        subscriptionService.broadcast(new SubscriptionMessage(SubscriptionMessageType.FORCE_UPDATE_STATE, null));
    }

    public GameState getGameInfo() {
        boolean isGameStarted = redisRepository.isGameStarted();
        boolean isGameEnded = redisRepository.isGameEnded();
        boolean freeze = redisRepository.isFreeze();
        int roundNum = redisRepository.getRoundNum();

        ZoneOffset offset = OffsetDateTime.now().getOffset();
        OffsetDateTime roundStart = currentRoundStart != null ? currentRoundStart.atOffset(offset) : null;
        return new GameState(isGameStarted, isGameEnded, roundNum, freeze, roundStart);
    }

    private OffsetDateTime getOffsetCurrentRoundStart() {
        ZoneOffset offset = OffsetDateTime.now().getOffset();
        return currentRoundStart.atOffset(offset);
    }
}
