package ru.serega6531.adplatf.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.serega6531.adplatf.configuration.PlatformConfiguration;

import java.util.List;
import java.util.Optional;

@Service
public class ConfigurationUtils {

    private final PlatformConfiguration configuration;

    @Autowired
    public ConfigurationUtils(PlatformConfiguration configuration) {
        this.configuration = configuration;
    }

    public Optional<Integer> findTeamIdByToken(String token) {
        List<PlatformConfiguration.Team> teams = configuration.getTeams();
        for (int i = 0; i < teams.size(); i++) {
            if (teams.get(i).getToken().equals(token)) {
                return Optional.of(i);
            }
        }

        return Optional.empty();
    }

}
