package ru.serega6531.adplatf.service;

import lombok.Synchronized;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.serega6531.adplatf.FlagSubmissionResult;
import ru.serega6531.adplatf.ServiceStatus;
import ru.serega6531.adplatf.configuration.PlatformConfiguration;
import ru.serega6531.adplatf.entity.Flag;
import ru.serega6531.adplatf.entity.TeamServiceStatus;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SubmissionService {

    private final FlagService flagService;
    private final StatusService statusService;
    private final ScoreService scoreService;
    private final CachingRedisRepository redisRepository;
    private final PlatformConfiguration configuration;
    private final ConfigurationUtils configurationUtils;

    @Autowired
    public SubmissionService(FlagService flagService, StatusService statusService,
                             ScoreService scoreService,
                             CachingRedisRepository redisRepository, PlatformConfiguration configuration,
                             ConfigurationUtils configurationUtils) {
        this.flagService = flagService;
        this.statusService = statusService;
        this.scoreService = scoreService;
        this.redisRepository = redisRepository;
        this.configuration = configuration;
        this.configurationUtils = configurationUtils;
    }

    @Synchronized
    public List<FlagSubmissionResult> submitFlags(List<String> flags, String token) {
        return flags.stream()
                .map(flag -> new FlagSubmissionResult(flag, submitFlag(flag, token)))
                .collect(Collectors.toList());
    }

    public String submitFlag(String flagText, String token) {
        if (token == null) {
            log.info("Someone tried to submit flag {}, but provided no team token", flagText);
            return "Team token not set";
        }

        Optional<Integer> teamOpt = configurationUtils.findTeamIdByToken(token);
        if (teamOpt.isEmpty()) {
            log.info("Someone tried to submit flag {}, but provided invalid team token {}", flagText, token);
            return "Invalid team token";
        }

        int attacker = teamOpt.get();
        String attackerName = configuration.getTeams().get(attacker).getName();
        String result = processFlag(flagText, attacker);

        log.info("Team {} submitted flag {}. Result: {}", attackerName, flagText, result);
        return result;
    }

    /**
     * Should follow ructf format (https://github.com/DestructiveVoice/DestructiveFarm/blob/master/server/protocols/ructf_http.py)
     */
    private String processFlag(String flagText, int attacker) {
        if (!redisRepository.isGameStarted()) {
            return "Game not started";
        }

        if (redisRepository.isGameEnded()) {
            return "Game over";
        }

        if (!flagService.isValidFlagFormat(flagText)) {
            return "Invalid flag";
        }

        Optional<Flag> flagOptional = flagService.getFlag(flagText);
        if (flagOptional.isEmpty()) {
            return "No such flag";
        }

        Flag flag = flagOptional.get();
        if (flag.getRedeems().contains(attacker)) {
            return "Flag already submitted";
        }

        int victim = flag.getTeam();

        if (victim == attacker) {
            return "This is your own flag";
        }

        int flagRound = flag.getRound();
        int currentRound = redisRepository.getRoundNum();
        if (currentRound - flagRound - 1 > configuration.getFlagLifeLength()) {
            return "Flag already expired";
        }

        if (configuration.isServiceMustBeUpToSubmit()) {
            TeamServiceStatus status = statusService.getCurrentStatus(attacker, flag.getService());
            if (status.getStatus() != ServiceStatus.UP) {
                return "Your service " + configuration.getServices().get(flag.getService()).getName() + " is not up";
            }
        }

        Pair<Double, Double> deltas = scoreService.stealFlag(attacker, victim, flag.getService());
        flagService.redeemFlag(flag, attacker);

        return String.format("Flag accepted. You got %.3f points. Victim lost %.3f points",
                deltas.getLeft(), -deltas.getValue());
    }

}
