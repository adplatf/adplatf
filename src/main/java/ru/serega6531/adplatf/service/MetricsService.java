package ru.serega6531.adplatf.service;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Timer;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.stereotype.Service;
import ru.serega6531.adplatf.amqp.CheckAction;
import ru.serega6531.adplatf.configuration.PlatformConfiguration;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class MetricsService {

    private final PlatformConfiguration configuration;

    // Map<Triple<Team, Service, Action>, Tracked object>
    // Only updates during initialization, so no need for ConcurrentHashMap
    private final Map<Triple<String, String, CheckAction>, AtomicInteger> executionTimes = new HashMap<>();
    private final Timer responseTimer;
    private final Counter checkerTimeoutCounter;

    public MetricsService(MeterRegistry registry, PlatformConfiguration configuration) {
        this.configuration = configuration;

        List<PlatformConfiguration.Team> teams = configuration.getTeams();
        List<PlatformConfiguration.Service> services = configuration.getServices();
        CheckAction[] actions = CheckAction.values();

        for (PlatformConfiguration.Team team : teams) {
            for (PlatformConfiguration.Service service : services) {
                for (CheckAction action : actions) {
                    Triple<String, String, CheckAction> key = ImmutableTriple.of(team.getName(), service.getName(), action);
                    List<Tag> tagList = List.of(Tag.of("team", team.getName()),
                            Tag.of("service", service.getName()),
                            Tag.of("action", action.getName()));

                    AtomicInteger executionTime = new AtomicInteger();
                    registry.gauge("checker_execution", tagList, executionTime);
                    executionTimes.put(key, executionTime);
                }
            }
        }

        this.responseTimer = Timer.builder("checker_response")
                .distributionStatisticExpiry(Duration.of(configuration.getCheckInterval(), ChronoUnit.SECONDS))
                .register(registry);

        this.checkerTimeoutCounter = registry.counter("checker_timeout");
    }

    public void recordExecutionTime(int team, int service, CheckAction action, int executionTime) {
        String teamName = configuration.getTeams().get(team).getName();
        String serviceName = configuration.getServices().get(service).getName();

        Triple<String, String, CheckAction> key = ImmutableTriple.of(teamName, serviceName, action);
        executionTimes.get(key).set(executionTime);
    }

    public void recordResponseTime(int responseTime) {
        responseTimer.record(responseTime, TimeUnit.MILLISECONDS);
    }

    public void incrementCheckerTimeouts() {
        checkerTimeoutCounter.increment();
    }

}
