package ru.serega6531.adplatf.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import ru.serega6531.adplatf.ServiceStatus;
import ru.serega6531.adplatf.configuration.PlatformConfiguration;
import ru.serega6531.adplatf.controller.pojo.ServiceStatusInfo;
import ru.serega6531.adplatf.controller.pojo.admin.StatusListRequest;
import ru.serega6531.adplatf.entity.TeamServiceStatus;
import ru.serega6531.adplatf.entity.TeamServiceStatusId;
import ru.serega6531.adplatf.repository.TeamServiceStatusRepository;
import ru.serega6531.adplatf.ws.SubscriptionMessage;
import ru.serega6531.adplatf.ws.SubscriptionMessageType;
import ru.serega6531.adplatf.ws.UpdateStatusMessageContent;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Slf4j
public class StatusService {

    public static final int DESCRIPTION_MAX_LENGTH = 1024;

    private final PlatformConfiguration platformConfiguration;
    private final TeamServiceStatusRepository repository;
    private final PlatformConfiguration configuration;
    private final JedisPool jedisPool;
    private final SubscriptionService subscriptionService;
    private final CachingRedisRepository redisRepository;

    // Map<Pair<Team, Service>, Status>
    private final Map<Pair<Integer, Integer>, TeamServiceStatus> currentStatuses = new ConcurrentHashMap<>();

    @Autowired
    public StatusService(PlatformConfiguration platformConfiguration, TeamServiceStatusRepository repository,
                         PlatformConfiguration configuration, JedisPool jedisPool, SubscriptionService subscriptionService,
                         CachingRedisRepository redisRepository) {
        this.platformConfiguration = platformConfiguration;
        this.repository = repository;
        this.configuration = configuration;
        this.jedisPool = jedisPool;
        this.subscriptionService = subscriptionService;
        this.redisRepository = redisRepository;
    }

    @PostConstruct
    public void init() {
        List<PlatformConfiguration.Team> teams = platformConfiguration.getTeams();
        List<PlatformConfiguration.Service> services = platformConfiguration.getServices();

        try (Jedis jedis = jedisPool.getResource()) {
            for (int team = 0; team < teams.size(); team++) {
                for (int service = 0; service < services.size(); service++) {
                    TeamServiceStatus status = repository.findLatestByTeamAndService(team, service)
                            .orElse(getFallbackStatus(team, service));

                    currentStatuses.put(ImmutablePair.of(team, service), status);
                    redisRepository.initStatusDefaults(team, service, jedis);
                }
            }
        }
    }

    /**
     * @return Map&lt;Team, Map&lt;Service, ServiceStatusInfo&gt;&gt;
     */
    public Map<Integer, Map<Integer, ServiceStatusInfo>> getAllStatuses(boolean bypassFreeze) {
        List<PlatformConfiguration.Team> teams = configuration.getTeams();
        List<PlatformConfiguration.Service> services = configuration.getServices();
        Map<Integer, Map<Integer, ServiceStatusInfo>> result = new HashMap<>();

        boolean freeze = redisRepository.isFreeze();
        boolean frozen = freeze && !bypassFreeze;

        for (int team = 0; team < teams.size(); team++) {
            Map<Integer, ServiceStatusInfo> teamResult = new HashMap<>();
            result.put(team, teamResult);

            for (int service = 0; service < services.size(); service++) {
                TeamServiceStatus status = getCurrentStatus(team, service);

                double sla;
                if (frozen) {
                    sla = getFrozenSla(team, service);
                } else {
                    sla = getSla(team, service);
                }

                ServiceStatusInfo serviceInfo = new ServiceStatusInfo(status.getStatus(), status.getDescription(), sla);
                teamResult.put(service, serviceInfo);
            }
        }

        return result;
    }

    public TeamServiceStatus getCurrentStatus(int team, int service) {
        return currentStatuses.get(ImmutablePair.of(team, service));
    }

    public void updateStatus(int team, int service, ServiceStatus status, String description, String error) {
        String teamName = platformConfiguration.getTeams().get(team).getName();
        String serviceName = platformConfiguration.getServices().get(service).getName();

        log.debug("Updating status for team {}, service {} to {} with description: \"{}\", error \"{}\"",
                teamName, serviceName, status.name(), cleanOutput(description), cleanOutput(error));

        TeamServiceStatusId id = new TeamServiceStatusId();
        id.setTeam(team);
        id.setService(service);
        id.setTime(LocalDateTime.now());

        TeamServiceStatus statusEntity = new TeamServiceStatus();
        statusEntity.setId(id);
        statusEntity.setStatus(status);
        statusEntity.setDescription(description);
        statusEntity.setError(error);

        repository.save(statusEntity);
        currentStatuses.put(ImmutablePair.of(team, service), statusEntity);

        long successfulChecks;
        long totalChecks;

        try (Jedis jedis = jedisPool.getResource()) {
            if (status == ServiceStatus.UP) {
                successfulChecks = redisRepository.incrementSuccessfulChecks(team, service, jedis);
            } else {
                successfulChecks = redisRepository.getSuccessfulChecks(team, service);
            }
            totalChecks = redisRepository.incrementTotalChecks(team, service, jedis);
        }

        if (redisRepository.isFreeze()) {
            successfulChecks = redisRepository.getFrozenSuccessfulChecks(team, service);
            totalChecks = redisRepository.getFrozenTotalChecks(team, service);
        }

        double sla;

        if (totalChecks != 0) {
            sla = (double) successfulChecks / totalChecks;
        } else {
            sla = 1;
        }

        subscriptionService.broadcast(new SubscriptionMessage(SubscriptionMessageType.UPDATE_STATUS,
                new UpdateStatusMessageContent(team, service, status, description, sla)));
    }

    public double getSla(int team, int service) {
        long successfulChecks = redisRepository.getSuccessfulChecks(team, service);
        long totalChecks = redisRepository.getTotalChecks(team, service);

        if (totalChecks == 0) {
            return 1;
        }

        return (double) successfulChecks / totalChecks;
    }

    public double getFrozenSla(int team, int service) {
        long successfulChecks = redisRepository.getFrozenSuccessfulChecks(team, service);
        long totalChecks = redisRepository.getFrozenTotalChecks(team, service);

        if (totalChecks == 0) {
            return 1;
        }

        return (double) successfulChecks / totalChecks;
    }

    public List<TeamServiceStatus> getStatuses(StatusListRequest request) {
        ZoneOffset offset = OffsetDateTime.now().getOffset();

        int page = request.getPage();
        int pageSize = request.getPageSize();
        int team = request.getTeam();
        int service = request.getService();
        LocalDateTime starting = request.getStarting().atZoneSameInstant(offset).toLocalDateTime();

        PageRequest pagination = PageRequest.of(page, pageSize, Sort.Direction.DESC, "id.time");
        return repository.findStatuses(team, service, starting, pagination);
    }

    private TeamServiceStatus getFallbackStatus(int team, int service) {
        TeamServiceStatusId id = new TeamServiceStatusId();
        id.setTeam(team);
        id.setService(service);
        id.setTime(LocalDateTime.now());

        TeamServiceStatus statusEntity = new TeamServiceStatus();
        statusEntity.setId(id);
        statusEntity.setStatus(ServiceStatus.DOWN);
        statusEntity.setDescription(null);
        statusEntity.setError(null);

        return statusEntity;
    }

    public String trimOutput(String output) {
        if (output.length() <= DESCRIPTION_MAX_LENGTH) {
            return output;
        }

        return output.substring(0, DESCRIPTION_MAX_LENGTH - 3) + "...";
    }

    public String cleanOutput(String output) {
        return output.replace("\n", " ");
    }

}
