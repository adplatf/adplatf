package ru.serega6531.adplatf.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.serega6531.adplatf.ServiceStatus;
import ru.serega6531.adplatf.amqp.CheckAction;
import ru.serega6531.adplatf.configuration.PlatformConfiguration;
import ru.serega6531.adplatf.entity.Flag;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Slf4j
public class CheckerService {

    private final PlatformConfiguration platformConfiguration;
    private final StatusService statusService;
    private final FlagService flagService;
    private final RunnerService scriptExecutorService;
    private final CachingRedisRepository redisRepository;

    // Map<Pair<Team, Service>, Time>
    private final Map<Pair<Integer, Integer>, LocalDateTime> lastUpdates = new ConcurrentHashMap<>();

    @Autowired
    public CheckerService(PlatformConfiguration platformConfiguration, StatusService statusService,
                          FlagService flagService, RunnerService scriptExecutorService, CachingRedisRepository redisRepository) {
        this.platformConfiguration = platformConfiguration;
        this.statusService = statusService;
        this.flagService = flagService;
        this.scriptExecutorService = scriptExecutorService;
        this.redisRepository = redisRepository;
    }

    public void runChecks(boolean withPush) {
        List<PlatformConfiguration.Service> services = platformConfiguration.getServices();
        List<PlatformConfiguration.Team> teams = platformConfiguration.getTeams();

        for (int service = 0; service < services.size(); service++) {
            for (int team = 0; team < teams.size(); team++) {
                runChecks(team, service, withPush);
            }
        }
    }

    public void runChecks(int team, int service, boolean withPush) {
        String teamName = platformConfiguration.getTeams().get(team).getName();
        String serviceName = platformConfiguration.getServices().get(service).getName();

        log.debug("[ROUND {}] Running checks for service {}, team {}", redisRepository.getRoundNum(), serviceName, teamName);

        scriptExecutorService.execute(team, service, CheckAction.CHECK, withPush, this::completeChecks, null);
    }

    private void completeChecks(int team, int service, String resultOut, String resultErr,
                                boolean withPush, LocalDateTime requestTime) {
        String teamName = platformConfiguration.getTeams().get(team).getName();
        String serviceName = platformConfiguration.getServices().get(service).getName();

        Optional<ServiceStatus> foundStatus = findStatusInString(resultOut);
        ServiceStatus status;

        if (foundStatus.isPresent()) {
            status = foundStatus.get();
        } else {
            log.warn("Status not found in response of check functionality script for team {}, service {}. " +
                            "Falling back to DOWN. Response: \"{}\". Error: \"{}\"", teamName, serviceName,
                    statusService.cleanOutput(resultOut), statusService.cleanOutput(resultErr));
            status = ServiceStatus.DOWN;
        }

        if (status == ServiceStatus.UP) {
            Flag currentFlag = flagService.getCurrentFlagOrGenerate(team, service, redisRepository.getRoundNum());
            boolean pushAgain = !currentFlag.isPushed();

            if (withPush) {
                pushFlag(team, service, currentFlag);
            } else if (pushAgain) {
                pushFlagAgain(team, service, currentFlag);
            } else {
                pullFlag(team, service, currentFlag);
            }
        } else {
            updateStatusIfRequired(team, service, status, resultOut, resultErr, requestTime);
        }
    }

    public void pullFlag(int team, int service, Flag currentFlag) {
        String teamName = platformConfiguration.getTeams().get(team).getName();
        String serviceName = platformConfiguration.getServices().get(service).getName();

        log.debug("[ROUND {}] Pulling flag for service {}, team {}", redisRepository.getRoundNum(), serviceName, teamName);

        scriptExecutorService.execute(team, service, CheckAction.PULL, false,
                this::completePull, currentFlag.getText());
    }

    private void completePull(int team, int service, String resultOut, String resultErr,
                              boolean withPush, LocalDateTime requestTime) {
        String teamName = platformConfiguration.getTeams().get(team).getName();
        String serviceName = platformConfiguration.getServices().get(service).getName();
        Optional<ServiceStatus> foundStatus = findStatusInString(resultOut);
        ServiceStatus status;

        if (foundStatus.isPresent()) {
            status = foundStatus.get();
        } else {
            log.warn("Status not found in response of pull flag script for team {}, service {}. " +
                            "Falling back to DOWN. Response: \"{}\". Error: \"{}\"", teamName, serviceName,
                    statusService.cleanOutput(resultOut), statusService.cleanOutput(resultErr));
            status = ServiceStatus.DOWN;
        }

        updateStatusIfRequired(team, service, status, resultOut, resultErr, requestTime);
    }

    public void pushFlag(int team, int service, Flag flag) {
        String teamName = platformConfiguration.getTeams().get(team).getName();
        String serviceName = platformConfiguration.getServices().get(service).getName();

        log.debug("[ROUND {}] Pushing flag for service {}, team {}", redisRepository.getRoundNum(), serviceName, teamName);

        scriptExecutorService.execute(team, service, CheckAction.PUSH, false,
                this::completePush, flag.getText());
    }

    public void pushFlagAgain(int team, int service, Flag flag) {
        String teamName = platformConfiguration.getTeams().get(team).getName();
        String serviceName = platformConfiguration.getServices().get(service).getName();

        log.debug("[ROUND {}] Pushing flag again for service {}, team {}", redisRepository.getRoundNum(), serviceName, teamName);

        scriptExecutorService.execute(team, service, CheckAction.PUSH, false,
                this::completePush, flag.getText());
    }

    private void completePush(int team, int service, String resultOut, String resultErr,
                              boolean withPush, LocalDateTime requestTime) {
        String teamName = platformConfiguration.getTeams().get(team).getName();
        String serviceName = platformConfiguration.getServices().get(service).getName();

        Optional<ServiceStatus> foundStatus = findStatusInString(resultOut);
        if (foundStatus.isPresent()) {
            log.trace("[ROUND {}] Flag pushed to team {} for service {}", redisRepository.getRoundNum(), teamName, serviceName);

            ServiceStatus status = foundStatus.get();
            if (status == ServiceStatus.UP) {
                Optional<Flag> currentFlag = flagService.getCurrentFlag(team, service);
                currentFlag.ifPresent(flagService::markFlagPushed);
            }

            updateStatusIfRequired(team, service, status, resultOut, resultErr, requestTime);
        } else {
            log.warn("Status not found in response of push flag script for team {}, service {}." +
                            " Response: \"{}\". Error: \"{}\"",
                    teamName, serviceName,
                    statusService.cleanOutput(resultOut), statusService.cleanOutput(resultErr));

            updateStatusIfRequired(team, service, ServiceStatus.DOWN, resultOut, resultErr, requestTime);
        }
    }

    private void updateStatusIfRequired(int team, int service, ServiceStatus status,
                                        String description, String error, LocalDateTime requestTime) {
        ImmutablePair<Integer, Integer> key = ImmutablePair.of(team, service);

        if (!lastUpdates.containsKey(key) || lastUpdates.get(key).isBefore(requestTime)) {
            statusService.updateStatus(team, service, status, description, error);
            lastUpdates.put(key, requestTime);
        }
    }

    private Optional<ServiceStatus> findStatusInString(String s) {
        return Arrays.stream(ServiceStatus.values())
                .filter(status -> StringUtils.containsIgnoreCase(s, status.name()))
                .findFirst();
    }

}
