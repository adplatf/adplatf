package ru.serega6531.adplatf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CtfAdPlatformApplication {

    public static void main(String[] args) {
        SpringApplication.run(CtfAdPlatformApplication.class, args);
    }

}
