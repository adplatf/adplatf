package ru.serega6531.adplatf.scheduler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.serega6531.adplatf.configuration.PlatformConfiguration;
import ru.serega6531.adplatf.service.*;

import java.time.LocalDateTime;

@Service
@Slf4j
public class GameScheduler {

    private final PlatformConfiguration platformConfiguration;
    private final CheckerService checkerService;
    private final GameStateService stateService;
    private final CachingRedisRepository redisRepository;

    @Autowired
    public GameScheduler(PlatformConfiguration platformConfiguration, CheckerService checkerService,
                         GameStateService stateService, CachingRedisRepository redisRepository) {
        this.platformConfiguration = platformConfiguration;
        this.checkerService = checkerService;
        this.stateService = stateService;
        this.redisRepository = redisRepository;

        log.info("Current time is {}, game starting at {}, game started = {}, game ended = {}," +
                        " score frozen = {}, currentRound = {}",
                LocalDateTime.now(), platformConfiguration.getGameStartTime(),
                redisRepository.isGameStarted(), redisRepository.isGameEnded(),
                redisRepository.isFreeze(), redisRepository.getRoundNum());
    }

    @Scheduled(cron = "0/#{platformConfiguration.checkInterval} * * ? * *")  // every N seconds starting from 00
    public void checkerTick() {
        if (redisRepository.isGameEnded()) {
            return;
        }

        LocalDateTime now = LocalDateTime.now();

        if (redisRepository.isGameStarted()) {
            log.info("Checker tick");

            LocalDateTime freezeTime = platformConfiguration.getFreezeStartTime();
            if (freezeTime != null && now.isAfter(freezeTime) && !redisRepository.isFreeze()) {
                stateService.startFreeze();
            }

            if (redisRepository.getCurrentCheckInRound() >= platformConfiguration.getChecksInRound()) {
                int currentRound = redisRepository.getRoundNum();
                if (currentRound == platformConfiguration.getTotalRounds()) {
                    // Ending game
                    stateService.endGame();
                    stateService.stopFreeze();
                } else {
                    // New round
                    stateService.beginNewRound();
                }
            } else {
                // Just another check
                runChecks();
            }
        } else {
            LocalDateTime startTime = platformConfiguration.getGameStartTime();

            if (now.isAfter(startTime)) {
                // Starting game
                stateService.startGame();
            }
        }
    }

    private void runChecks() {
        redisRepository.setCurrentCheckInRound(redisRepository.getCurrentCheckInRound() + 1);
        checkerService.runChecks(false);
    }

}
