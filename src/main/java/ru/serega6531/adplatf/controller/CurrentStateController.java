package ru.serega6531.adplatf.controller;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.serega6531.adplatf.configuration.ClientConfiguration;
import ru.serega6531.adplatf.controller.pojo.CurrentState;
import ru.serega6531.adplatf.controller.pojo.GameState;
import ru.serega6531.adplatf.controller.pojo.ServiceInfo;
import ru.serega6531.adplatf.controller.pojo.ServiceStatusInfo;
import ru.serega6531.adplatf.service.GameStateService;
import ru.serega6531.adplatf.service.ScoreService;
import ru.serega6531.adplatf.service.StatusService;

import java.util.Map;

@RestController
public class CurrentStateController {

    private final GameStateService stateService;
    private final ScoreService scoreService;
    private final StatusService statusService;
    private final ObjectFactory<ClientConfiguration> clientConfiguration;

    public CurrentStateController(GameStateService stateService, ScoreService scoreService, StatusService statusService,
                                  ObjectFactory<ClientConfiguration> clientConfiguration) {
        this.stateService = stateService;
        this.scoreService = scoreService;
        this.statusService = statusService;
        this.clientConfiguration = clientConfiguration;
    }

    @GetMapping("/state")
    @CrossOrigin
    public CurrentState getState() {
        GameState gameInfo = stateService.getGameInfo();
        Map<Integer, Map<Integer, ServiceInfo>> scores = scoreService.getAllScores(false);
        Map<Integer, Map<Integer, ServiceStatusInfo>> statuses = statusService.getAllStatuses(false);
        Map<Integer, Integer> firstBloods = scoreService.getFirstBloods();

        return new CurrentState(clientConfiguration.getObject(), gameInfo, scores, statuses, firstBloods);
    }

}
