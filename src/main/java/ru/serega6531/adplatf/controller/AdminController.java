package ru.serega6531.adplatf.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.web.bind.annotation.*;
import ru.serega6531.adplatf.configuration.ClientConfiguration;
import ru.serega6531.adplatf.configuration.PlatformConfiguration;
import ru.serega6531.adplatf.controller.pojo.CurrentState;
import ru.serega6531.adplatf.controller.pojo.GameState;
import ru.serega6531.adplatf.controller.pojo.ServiceInfo;
import ru.serega6531.adplatf.controller.pojo.ServiceStatusInfo;
import ru.serega6531.adplatf.controller.pojo.admin.StatusListRequest;
import ru.serega6531.adplatf.controller.pojo.admin.TeamServiceStatusModel;
import ru.serega6531.adplatf.entity.TeamServiceStatus;
import ru.serega6531.adplatf.service.GameStateService;
import ru.serega6531.adplatf.service.ScoreService;
import ru.serega6531.adplatf.service.StatusService;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin-api")
@CrossOrigin
@Slf4j
public class AdminController {

    private final GameStateService stateService;
    private final ScoreService scoreService;
    private final StatusService statusService;
    private final PlatformConfiguration configuration;
    private final ObjectFactory<ClientConfiguration> clientConfiguration;

    public AdminController(GameStateService stateService, ScoreService scoreService, StatusService statusService,
                           PlatformConfiguration configuration, ObjectFactory<ClientConfiguration> clientConfiguration) {
        this.stateService = stateService;
        this.scoreService = scoreService;
        this.statusService = statusService;
        this.configuration = configuration;
        this.clientConfiguration = clientConfiguration;
    }

    @GetMapping("/state")
    public CurrentState getAdminState() {
        GameState gameInfo = stateService.getGameInfo();
        Map<Integer, Map<Integer, ServiceInfo>> scores = scoreService.getAllScores(true);
        Map<Integer, Map<Integer, ServiceStatusInfo>> statuses = statusService.getAllStatuses(true);

        return new CurrentState(clientConfiguration.getObject(), gameInfo, scores, statuses, Collections.emptyMap());
    }

    @PostMapping("/statuses")
    public List<TeamServiceStatusModel> getStatuses(@RequestBody StatusListRequest request) {
        List<TeamServiceStatus> statuses = statusService.getStatuses(request);
        return statuses.stream()
                .map(this::statusToModel)
                .collect(Collectors.toList());
    }

    @GetMapping("/config")
    public PlatformConfiguration getConfig() {
        return configuration;
    }

    @PostMapping("/startGame")
    public void startGame() {
        stateService.startGame();
    }

    @PostMapping("/endGame")
    public void endGame() {
        stateService.endGame();
    }

    @PostMapping("/startFreeze")
    public void startFreeze() {
        stateService.startFreeze();
    }

    @PostMapping("/stopFreeze")
    public void stopFreeze() {
        stateService.stopFreeze();
    }

    @PostMapping("/setScore/{team}/{service}")
    public void setScore(@PathVariable int team, @PathVariable int service, @RequestBody double newScore) {
        if (newScore < 0) {
            return;
        }

        stateService.forceSetScore(team, service, newScore);
    }

    @PostMapping("/setStolenFlags/{team}/{service}")
    public void setStolenFlags(@PathVariable int team, @PathVariable int service, @RequestBody int newAmount) {
        if (newAmount < 0) {
            return;
        }

        stateService.forceSetStolenFlags(team, service, newAmount);
    }

    @PostMapping("/setLostFlags/{team}/{service}")
    public void setLostFlags(@PathVariable int team, @PathVariable int service, @RequestBody int newAmount) {
        if (newAmount < 0) {
            return;
        }

        stateService.forceSetLostFlags(team, service, newAmount);
    }

    @PostMapping("/setNextRound")
    public void setNextRound(@RequestBody int nextRound) {
        if (nextRound < 1 || nextRound > configuration.getTotalRounds()) {
            return;
        }

        stateService.forceSetNextRound(nextRound);
    }

    @PostMapping("/setCtfTitle")
    public void setCtfTitle(@RequestBody String ctfTitle) {
        configuration.setCtfTitle(ctfTitle);
        stateService.sendUpdateState();
        log.info("Updated ctfTitle to {}", ctfTitle);
    }

    @PostMapping("/setTotalRounds")
    public void setTotalRounds(@RequestBody int totalRounds) {
        configuration.setTotalRounds(totalRounds);
        stateService.sendUpdateState();
        log.info("Updated totalRounds to {}", totalRounds);
    }

    @PostMapping("/setChecksInRound")
    public void setChecksInRound(@RequestBody int checksInRound) {
        configuration.setChecksInRound(checksInRound);
        stateService.sendUpdateState();
        log.info("Updated checksInRound to {}", checksInRound);
    }

    @PostMapping("/setFlagLifeLength")
    public void setFlagLifeLength(@RequestBody int flagLifeLength) {
        configuration.setFlagLifeLength(flagLifeLength);
        stateService.sendUpdateState();
        log.info("Updated flagLifeLength to {}", flagLifeLength);
    }

    @PostMapping("/setGameStartTime")
    public void setGameStartTime(@RequestBody LocalDateTime gameStartTime) {
        configuration.setGameStartTime(gameStartTime);
        stateService.sendUpdateState();
        log.info("Updated gameStartTime to {}", gameStartTime);
    }

    @PostMapping("/setFreezeStartTime")
    public void setFreezeStartTime(@RequestBody LocalDateTime freezeStartTime) {
        configuration.setFreezeStartTime(freezeStartTime);
        stateService.sendUpdateState();
        log.info("Updated freezeStartTime to {}", freezeStartTime);
    }

    @PostMapping("/setTeamName/{team}")
    public void setTeamName(@PathVariable int team, @RequestBody String name) {
        PlatformConfiguration.Team configTeam = configuration.getTeams().get(team);
        String oldName = configTeam.getName();
        configTeam.setName(name);
        stateService.sendUpdateState();
        log.info("Updated team name from {} to {}", oldName, name);
    }

    @PostMapping("/setTeamIcon/{team}")
    public void setTeamIcon(@PathVariable int team, @RequestBody String icon) {
        PlatformConfiguration.Team configTeam = configuration.getTeams().get(team);
        String name = configTeam.getName();
        String oldIcon = configTeam.getIcon();
        configTeam.setIcon(icon);
        stateService.sendUpdateState();
        log.info("Updated icon of team {} from {} to {}", name, oldIcon, icon);
    }

    @PostMapping("/setTeamToken/{team}")
    public void setTeamToken(@PathVariable int team, @RequestBody String token) {
        PlatformConfiguration.Team configTeam = configuration.getTeams().get(team);
        String name = configTeam.getName();
        String oldToken = configTeam.getToken();
        configTeam.setToken(token);
        stateService.sendUpdateState();
        log.info("Updated token of team {} from {} to {}", name, oldToken, token);
    }

    @PostMapping("/setServiceName/{service}")
    public void setServiceName(@PathVariable int service, @RequestBody String name) {
        PlatformConfiguration.Service configService = configuration.getServices().get(service);
        String oldName = configService.getName();
        configService.setName(name);
        stateService.sendUpdateState();
        log.info("Updated service name from {} to {}", oldName, name);
    }

    private TeamServiceStatusModel statusToModel(TeamServiceStatus status) {
        ZoneOffset offset = OffsetDateTime.now().getOffset();
        return new TeamServiceStatusModel(
                status.getId().getTime().atOffset(offset),
                status.getStatus(),
                status.getDescription(),
                status.getError());
    }

}
