package ru.serega6531.adplatf.controller;

import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import ru.serega6531.adplatf.FlagSubmissionResult;
import ru.serega6531.adplatf.service.SubmissionService;

import java.util.List;

@RestController
public class SubmissionController {

    private final SubmissionService submissionService;

    public SubmissionController(SubmissionService submissionService) {
        this.submissionService = submissionService;
    }

    @PutMapping("/flags")
    public List<FlagSubmissionResult> submitFlags(@RequestBody List<String> flags,
                                                  @RequestHeader(value = "X-Team-Token", required = false) String token) {
        return submissionService.submitFlags(flags, token);
    }

}
