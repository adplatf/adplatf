package ru.serega6531.adplatf.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.AbstractWebSocketHandler;
import ru.serega6531.adplatf.service.SubscriptionService;

@Component
public class SubscribingWebSocketHandler extends AbstractWebSocketHandler {

    private final SubscriptionService subscriptionService;

    @Autowired
    public SubscribingWebSocketHandler(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        subscriptionService.addSubscriber(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        subscriptionService.removeSubscriber(session);
    }

}
