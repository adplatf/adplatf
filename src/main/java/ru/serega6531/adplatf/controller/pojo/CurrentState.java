package ru.serega6531.adplatf.controller.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.serega6531.adplatf.configuration.ClientConfiguration;

import java.util.Map;

@Data
@AllArgsConstructor
public class CurrentState {

    private ClientConfiguration config;
    private GameState state;

    // Map<Team, Map<Service, ServiceInfo>>
    private Map<Integer, Map<Integer, ServiceInfo>> scores;
    private Map<Integer, Map<Integer, ServiceStatusInfo>> statuses;

    private Map<Integer, Integer> firstBloods;
}
