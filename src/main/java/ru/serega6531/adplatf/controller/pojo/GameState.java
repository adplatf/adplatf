package ru.serega6531.adplatf.controller.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.OffsetDateTime;

@Data
@AllArgsConstructor
public class GameState {

    private boolean gameStarted;
    private boolean gameEnded;
    private int currentRound;
    private boolean freeze;
    private OffsetDateTime currentRoundStart;

}
