package ru.serega6531.adplatf.controller.pojo.admin;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.serega6531.adplatf.ServiceStatus;

import java.time.OffsetDateTime;

@Data
@AllArgsConstructor
public class TeamServiceStatusModel {

    private OffsetDateTime time;
    private ServiceStatus status;
    private String description;
    private String error;

}
