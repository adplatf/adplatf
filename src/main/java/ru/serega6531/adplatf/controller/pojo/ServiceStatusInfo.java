package ru.serega6531.adplatf.controller.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.serega6531.adplatf.ServiceStatus;

@Data
@AllArgsConstructor
public class ServiceStatusInfo {

    private ServiceStatus status;
    private String description;
    private double sla;

}
