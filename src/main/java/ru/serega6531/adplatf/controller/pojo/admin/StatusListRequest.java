package ru.serega6531.adplatf.controller.pojo.admin;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class StatusListRequest {

    private OffsetDateTime starting;
    private int page;
    private int pageSize;
    private int team;
    private int service;

}
