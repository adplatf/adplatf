package ru.serega6531.adplatf.controller.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ServiceInfo {

    private double score;
    private int stolenFlags;
    private int lostFlags;

}