package ru.serega6531.adplatf.entity;

import lombok.Data;

import javax.persistence.Embeddable;
import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Embeddable
@Data
public class TeamServiceStatusId implements Serializable {

    @Serial
    private static final long serialVersionUID = 7032806212632237814L;

    private Integer team;
    private Integer service;
    private LocalDateTime time;

}
