package ru.serega6531.adplatf.entity;

import lombok.Data;
import ru.serega6531.adplatf.ServiceStatus;
import ru.serega6531.adplatf.service.StatusService;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
@Data
public class TeamServiceStatus {

    @EmbeddedId
    private TeamServiceStatusId id;

    private ServiceStatus status;

    @Column(length = StatusService.DESCRIPTION_MAX_LENGTH)
    private String description;

    @Column(columnDefinition = "TEXT")
    private String error;

}
