package ru.serega6531.adplatf.entity;

import lombok.Data;
import ru.serega6531.adplatf.service.FlagService;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
public class Flag {

    @Id
    @Column(columnDefinition = "char(" + FlagService.FLAG_LENGTH + ")")
    private String text;

    private Integer team;
    private Integer service;
    private int round;
    private boolean pushed;

    @ElementCollection
    @CollectionTable(name = "flag_redeems")
    private Set<Integer> redeems = new HashSet<>();

}
