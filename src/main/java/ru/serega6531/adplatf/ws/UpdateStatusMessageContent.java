package ru.serega6531.adplatf.ws;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.serega6531.adplatf.ServiceStatus;

@AllArgsConstructor
@Data
public class UpdateStatusMessageContent implements SubscriptionMessageContent {

    private int team;
    private int service;
    private ServiceStatus status;
    private String description;
    private double sla;

}
