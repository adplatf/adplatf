package ru.serega6531.adplatf.ws;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SubscriptionMessage {

    private SubscriptionMessageType type;
    private SubscriptionMessageContent value;

}
