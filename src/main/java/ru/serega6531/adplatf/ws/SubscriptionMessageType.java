package ru.serega6531.adplatf.ws;

public enum SubscriptionMessageType {

    UPDATE_STATUS, UPDATE_SCORE, STEAL_FLAG, GAME_START, GAME_END, NEW_ROUND, FREEZE_START, FIRST_BLOOD,
    FORCE_UPDATE_STATE // make client retrieve the whole state, usually after interruption from the admin

}
