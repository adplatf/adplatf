package ru.serega6531.adplatf.ws;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StealFlagMessageContent implements SubscriptionMessageContent {

    private int thiefTeam;
    private int victimTeam;
    private int service;
    private double attackerDelta;
    private double victimDelta;

}
