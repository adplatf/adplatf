package ru.serega6531.adplatf.ws;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.OffsetDateTime;

@Data
@AllArgsConstructor
public class NewRoundMessageContent implements SubscriptionMessageContent {

    private int roundNum;
    private OffsetDateTime roundStart;

}
