package ru.serega6531.adplatf.ws;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FirstBloodMessageContent implements SubscriptionMessageContent {

    private int team;
    private int service;

}
