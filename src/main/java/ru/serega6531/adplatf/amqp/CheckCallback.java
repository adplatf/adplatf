package ru.serega6531.adplatf.amqp;

import java.time.LocalDateTime;

@FunctionalInterface
public interface CheckCallback {

    void call(int team, int service, String resultOut, String resultErr, boolean withPush, LocalDateTime requestTime);

}
