package ru.serega6531.adplatf.amqp;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class CheckerRequest implements Serializable {

    @Serial
    private static final long serialVersionUID = 1753838494702256670L;

    private int team;
    private int service;
    private CheckAction action;
    private boolean expectedPush;
    private LocalDateTime requestTime;
    private String flag;

}
